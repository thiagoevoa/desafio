RecyclerView - Foi feito a escolha desse componente pois diferente do ListView, é possível ter uma maior customização das formas de lista caso necessário.

Volley - Foi feito a escolha dessa biblioteca pois é oficial e está na documentação da Google, poderia ter sido feito a escolha do Retrofit, porém ambas são similares e possuem performance parecidas, que no caso não faria diferença.

GSON - Foi feito a escolha dessa biblioteca para fazer a conversão do Json em objetos na aplicação, não convem fazer isso manualmente.

Glide - Foi feito a escolha dessa biblioteca para fazer manippulação de imagem, pois além de ela realizar de forma abstratida, requisições a urls, uris ela também converte bloob e ainda dá a opção de customizar a forma de apresentar a imagem no componente e também dá a opção de guardar em cache.  

Anko - Foi feito a escolha dessa biblioteca para que pudesse fazer execuções fora da Mainthread (o que é aconcelhado pela Google, para que n atrapalhe a experiência do usuário), poderia ter optado por utilizar o Kotlin Coroutines, porém não iria fazer uma implementação já existente no mercado que é bastante utilizada pela comunidade.

Room - Foi feito a escolha de utilzar essa biblioteca do Architecture Components, pois além de ser nativa da plataforma, ela nos promove agilidade no desenvolvimento e performance no acesso ao SQLite. 
