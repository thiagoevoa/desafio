package com.example.desafio.dao

import android.arch.persistence.room.Room
import android.content.Context

class Database {
    fun getInstance(context: Context): AppDatabase{
        return Room.databaseBuilder(context, AppDatabase::class.java, "myDatabase")
            .fallbackToDestructiveMigration()
            .build()
    }
}