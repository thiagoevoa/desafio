package com.example.desafio.util

import android.content.Context
import android.widget.Toast
import com.example.desafio.model.Product
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun mountProducts(value: String?): MutableList<Product>{
    val list: MutableList<Product> = mutableListOf()
    val jsonArray = JSONArray(JSONObject(value).getString("products"))
    for(i in 0 until jsonArray.length()){
        val item = Gson().fromJson(jsonArray[i].toString(), Product::class.java)
        list.add(item)
    }
    return list
}