package com.example.desafio.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.desafio.R
import com.example.desafio.activity.MainActivity
import com.example.desafio.model.Product
import kotlinx.android.synthetic.main.adapter_item.view.*

class AdapterProduct(private val activity: Activity, private val products: MutableList<Product>): RecyclerView.Adapter<AdapterProduct.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_item, parent, false))
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(activity.baseContext).load(products[position].image).into(holder.imgProduct)
        holder.txtProduct.text = products[position].title

        holder.itemView.setOnClickListener {
            (activity as MainActivity).itemClicked(products[position])
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var imgProduct: ImageView = view.img_item
        var txtProduct: TextView = view.txt_item
    }
}