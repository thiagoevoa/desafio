package com.example.desafio.dao

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.desafio.model.Cart

@Database(entities = [Cart::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun cartDAO(): CartDAO
}