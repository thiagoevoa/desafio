package com.example.desafio.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.desafio.R
import com.example.desafio.dao.Database
import com.example.desafio.model.Cart
import com.example.desafio.model.Product
import com.example.desafio.util.BUNDLE_PRODUCT
import com.example.desafio.util.showToast
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetailActivity : AppCompatActivity() {
    private lateinit var product: Product
    private var quantity = 0
    private var chantily = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        product = intent.getParcelableExtra(BUNDLE_PRODUCT)

        initView()
    }

    private fun initView() {
        Glide.with(this).load(product.image).into(img_item)
        txt_item_name.text = product.title

        img_small.setOnClickListener {
            clearSize()
            txt_item_quantity.text = "${100} ML"
            it.alpha = 1F
        }

        img_medium.setOnClickListener {
            clearSize()
            txt_item_quantity.text = "${200} ML"
            it.alpha = 1F
        }

        img_big.setOnClickListener {
            clearSize()
            txt_item_quantity.text = "${400} ML"
            it.alpha = 1F
        }

        img_no_sugar.setOnClickListener {
            clearSugar()
            it.alpha = 1F
        }

        img_sugar_1.setOnClickListener {
            clearSugar()
            it.alpha = 1F
        }

        img_sugar_2.setOnClickListener {
            clearSugar()
            it.alpha = 1F
        }

        img_sugar_3.setOnClickListener {
            clearSugar()
            it.alpha = 1F
        }

        img_additional_1.setOnClickListener {
            when (chantily) {
                true -> {
                    it.alpha = 0.5F
                    chantily = false
                }
                else -> {
                    it.alpha = 1F
                    chantily = true
                }
            }
        }

        btn_less.setOnClickListener {
            when {
                quantity > 0 -> {
                    quantity--
                    txt_quantity.text = quantity.toString()
                }
                else -> {
                    quantity = 0
                    txt_quantity.text = quantity.toString()
                }
            }
        }

        btn_plus.setOnClickListener {
            txt_quantity.text = quantity++.toString()
        }

        btn_add_cart.setOnClickListener {
            when (quantity) {
                0 -> showToast(this, resources.getString(R.string.error_inform_quantity))
                else -> {
                    when (chantily) {
                        true -> product.additional?.add("chantily")
                    }

                    doAsync {

                        val result = Database().getInstance(this@DetailActivity).cartDAO().insert(
                            Cart(
                                null,
                                product.title,
                                quantity,
                                product.additional.toString(),
                                product.image
                            )
                        )
                        uiThread {
                            if (result > 0) {
                                showToast(this@DetailActivity, resources.getString(R.string.product_in_cart))
                                finish()
                            } else {
                                showToast(this@DetailActivity, resources.getString(R.string.error_product_in_cart))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun clearSize() {
        img_small.alpha = 0.5F
        img_medium.alpha = 0.5F
        img_big.alpha = 0.5F
    }

    private fun clearSugar() {
        img_no_sugar.alpha = 0.5F
        img_sugar_1.alpha = 0.5F
        img_sugar_2.alpha = 0.5F
        img_sugar_3.alpha = 0.5F
    }
}
