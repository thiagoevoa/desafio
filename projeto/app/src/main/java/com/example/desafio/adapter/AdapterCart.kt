package com.example.desafio.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.desafio.R
import com.example.desafio.model.Cart
import kotlinx.android.synthetic.main.adapter_cart.view.*

class AdapterCart(private val activity: Activity, private val cart: MutableList<Cart>): RecyclerView.Adapter<AdapterCart.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_cart, parent, false))
    }

    override fun getItemCount(): Int {
        return cart.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(activity.baseContext).load(cart[position].image).into(holder.imgProduct)
        holder.txtProductName.text = cart[position].productName
        holder.txtProductQuantity.text = cart[position].quantity.toString()
        holder.txtProductAdditional.text = cart[position].additional?.replace("[","")?.replace("]","")
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var txtProductName: TextView = view.txt_product_name
        var txtProductQuantity: TextView = view.txt_product_quantity
        var txtProductAdditional: TextView = view.txt_product_additional
        var imgProduct: ImageView = view.img_product
    }
}