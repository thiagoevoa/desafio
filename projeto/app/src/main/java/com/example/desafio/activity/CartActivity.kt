package com.example.desafio.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.desafio.R
import com.example.desafio.adapter.AdapterCart
import com.example.desafio.dao.Database
import kotlinx.android.synthetic.main.activity_cart.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        loadCartList()
    }

    private fun loadCartList(){
        doAsync {
            val products = Database().getInstance(this@CartActivity).cartDAO().listAll()
            uiThread {
                when{
                    products.size > 0 -> {
                        txt_message.visibility = View.GONE
                        recycler_cart.layoutManager = LinearLayoutManager(this@CartActivity)
                        recycler_cart.adapter = AdapterCart(this@CartActivity, products)
                    }
                    else -> txt_message.visibility = View.VISIBLE
                }
            }
        }
    }

}
