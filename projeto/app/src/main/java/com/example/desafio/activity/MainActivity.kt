package com.example.desafio.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.desafio.R
import com.example.desafio.adapter.AdapterProduct
import com.example.desafio.model.Product
import com.example.desafio.util.BUNDLE_PRODUCT
import com.example.desafio.util.URL_PRODUCTS
import com.example.desafio.util.mountProducts
import com.example.desafio.util.showToast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        retrieveProducts()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.action_cart -> startActivity(Intent(this, CartActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    fun itemClicked(product: Product){
        startActivity(Intent(this, DetailActivity::class.java)
            .putExtra(BUNDLE_PRODUCT, product))
    }

    private fun retrieveProducts(){
        Volley.newRequestQueue(this).add(object: StringRequest(
            Request.Method.GET,
            URL_PRODUCTS,
            Response.Listener {
                recycler_items.layoutManager = LinearLayoutManager(this)
                recycler_items.adapter = AdapterProduct(this, mountProducts(it))
            },
            Response.ErrorListener {
                showToast(this, resources.getString(R.string.error_retrieve_products))
            }
        ){})
    }
}
