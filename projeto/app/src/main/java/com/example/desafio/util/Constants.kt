package com.example.desafio.util

//URLS
const val URL_PRODUCTS = "https://desafio-mobility.herokuapp.com/products.json"

//BUNDLE
const val BUNDLE_PRODUCT = "product"
const val BUNDLE_QUANTITY = "product"
